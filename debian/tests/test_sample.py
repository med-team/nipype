import os
from nipype import Workflow, Node, Function

def sum(a, b):
    return a + b

def concat(a, b):
    return [a, b]

def test_basic():
    wf = Workflow('hello')
    adder = Node(Function(input_names=['a', 'b'],
                  output_names=['sum'],
                  function=sum),
                 name='a_plus_b')
    adder.inputs.a = 1
    adder.inputs.b = 3
    wf.add_nodes([adder])
    wf.base_dir = os.getcwd()
    concater = Node(Function(input_names=['a', 'b'],
                     output_names=['some_list'],
                     function=concat),
                    name='concat_a_b')
    wf.connect(adder, 'sum', concater, 'a')
    concater.inputs.b = 3
    eg = wf.run()

    output1_hash=list(eg.nodes())[0].result.outputs.get_hashval()[1]
    output2_hash=list(eg.nodes())[1].result.outputs.get_hashval()[1]

    assert output1_hash == '9195407a28ce18c50c76467f1223e032'
    assert output2_hash == '276ae6fb5717660a46cd790655ee33d4'
